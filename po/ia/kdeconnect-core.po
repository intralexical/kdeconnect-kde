# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# giovanni <g.sora@tiscali.it>, 2017, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-12 00:45+0000\n"
"PO-Revision-Date: 2021-11-15 22:32+0100\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: backends/bluetooth/bluetoothpairinghandler.cpp:56
#: backends/lan/lanpairinghandler.cpp:52
#, kde-format
msgid "Canceled by other peer"
msgstr "Cancellate per altere pare"

#: backends/bluetooth/bluetoothpairinghandler.cpp:65
#: backends/lan/lanpairinghandler.cpp:61
#, kde-format
msgid "%1: Already paired"
msgstr "%1: Jam coppiate"

#: backends/bluetooth/bluetoothpairinghandler.cpp:68
#, kde-format
msgid "%1: Pairing already requested for this device"
msgstr "%1: association (Pairing) ja requirite per iste dispositivo"

#: backends/bluetooth/bluetoothpairinghandler.cpp:124
#: backends/lan/lanpairinghandler.cpp:106
#, kde-format
msgid "Timed out"
msgstr "Expirate"

#: backends/lan/compositeuploadjob.cpp:82
#, kde-format
msgid "Couldn't find an available port"
msgstr "Non poteva trovar un porto disponibile"

#: backends/lan/compositeuploadjob.cpp:121
#, kde-format
msgid "Failed to send packet to %1"
msgstr "Il falleva a inviar pacchetto a %1"

#: backends/lan/compositeuploadjob.cpp:287
#, kde-format
msgid "Sending to %1"
msgstr "Inviante  a %1"

#: backends/lan/compositeuploadjob.cpp:287
#, kde-format
msgid "File"
msgstr "File"

#: backends/lan/landevicelink.cpp:146 backends/lan/landevicelink.cpp:160
#, kde-format
msgid ""
"This device cannot be paired because it is running an old version of KDE "
"Connect."
msgstr ""
"Iste dispositivo non poteva esser associate (paired) proque es executante "
"sur un vetere version de KDE Connect."

#: compositefiletransferjob.cpp:46
#, kde-format
msgctxt "@title job"
msgid "Receiving file"
msgid_plural "Receiving files"
msgstr[0] "Recipente file "
msgstr[1] "Recipente files"

#: compositefiletransferjob.cpp:47
#, kde-format
msgctxt "The source of a file operation"
msgid "Source"
msgstr "Fonte"

#: compositefiletransferjob.cpp:48
#, kde-format
msgctxt "The destination of a file operation"
msgid "Destination"
msgstr "Destination"

#: device.cpp:215
#, kde-format
msgid "Already paired"
msgstr "Jam coppiate"

#: device.cpp:220
#, kde-format
msgid "Device not reachable"
msgstr "Dispositivo non attingibile"

#: device.cpp:561
#, kde-format
msgid "SHA256 fingerprint of your device certificate is: %1\n"
msgstr "Impression digital SHA256 de tu certificato de dispositivo es: %1\n"

#: device.cpp:569
#, kde-format
msgid "SHA256 fingerprint of remote device certificate is: %1\n"
msgstr ""
"Impression digital SHA256 del certificato de dispositivo remote es : %1\n"

#: filetransferjob.cpp:51
#, kde-format
msgid "Filename already present"
msgstr "Nomine de file ja presente"

#: filetransferjob.cpp:100
#, kde-format
msgid "Received incomplete file: %1"
msgstr "File recipite incomplete: %1"

#: filetransferjob.cpp:118
#, kde-format
msgid "Received incomplete file from: %1"
msgstr "File recipite incomplete ex: %1"

#: kdeconnectconfig.cpp:76
#, kde-format
msgid "KDE Connect failed to start"
msgstr "KDE Connect fallave a initiar"

#: kdeconnectconfig.cpp:77
#, kde-format
msgid ""
"Could not find support for RSA in your QCA installation. If your "
"distribution provides separate packets for QCA-ossl and QCA-gnupg, make sure "
"you have them installed and try again."
msgstr ""
"Non pote trovar supporto per RSA in tu installation de QCA. Si tu "
"distribution forni pacchettos separate per qCA-ossl e QCA-gnupg, tu assecura "
"te que tu ha installate illos e essaya novemente."

#: kdeconnectconfig.cpp:313
#, kde-format
msgid "Could not store private key file: %1"
msgstr "Non pote immagazinar file de clave private: %1"

#: kdeconnectconfig.cpp:358
#, kde-format
msgid "Could not store certificate file: %1"
msgstr "Non pote immagazinar file de certificato : %1"

#~ msgid "Sent 1 file"
#~ msgid_plural "Sent %1 files"
#~ msgstr[0] "Inviate 1 file"
#~ msgstr[1] "Inviate %1 files"

#~ msgid "Progress"
#~ msgstr "Progresso"

#~ msgid "Sending file %1 of %2"
#~ msgstr "Inviante file %1 a %2"

#~ msgid "Receiving file %1 of %2"
#~ msgstr "Recipente file %1 de %2"

#~ msgctxt "File transfer origin"
#~ msgid "From"
#~ msgstr "Ex"

#~ msgctxt "File transfer destination"
#~ msgid "To"
#~ msgstr "A"
